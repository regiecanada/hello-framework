<?php

namespace app\model;

/**
 * Description of UserModel
 *
 * @author Regie
 */
class UserModel extends \rueckgrat\mvc\DefaultDBModel{
    public function __construct(){
        parent::__construct("user");
    }
    public function getAllUsers(){
        $stmnt = $this->db->query("SELECT * FROM user");
        
        while($row = $stmnt->fetch()){
            $user = new \app\mapper\User();
            $user->map($row);
            
            $users[] = $user; 
        }
        
        return $users;
    }
}
